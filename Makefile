OBJ = sux-spec.pdf
COVER = sux-cover.ms
SRC = sux-spec.ms

all: clean sux.pdf

sux.pdf: sux-spec.ms
	soelim $(SRC) | dformat.awk | pdfroff --stylesheet=$(COVER) -tp -mspdf - --pdf-output=$(OBJ)

clean:
	rm -f $(OBJ)
