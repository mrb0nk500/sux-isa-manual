\" Start of the introduction.
.NH
.XN Introduction
.LP
Sux
(Pronounced "sucks")
is a new, 64-bit CISC instruction-set architecture,
(ISA)
that was designed to be very simple,
and easy to understand.
Our goals in creating Sux include:
.IP \[bu] 2
A 100% totally free
(as in freedom)
ISA that both respects your freedoms,
and does not allow anyone to make proprietary versions of it.
.IP \[bu] 2
An ISA that is suitable for implementing in hardware,
not just emulation,
or binary translation.
.IP \[bu] 2
An ISA that is very simple,
both at the assembly language level,
and at the hardware level.
.IP \[bu] 2
An ISA that can be implemented with as
low a transistor count as possible,
and therefore, very cheap to produce.
.IP \[bu] 2
An ISA that can actually be understood by any programmer,
and is actually possible to write assembly language programs in.
.IP \[bu] 2
An ISA that is very memory efficient.
.IP \[bu] 2
An ISA that is extremely fast,
and allows for an insane number of cores.
.IP \[bu] 2
An ISA that does not require uneeded bloat,
just to make it "faster".
.IP \[bu] 2
An ISA that a hobbiest can use.
.IP \[bu] 2
An ISA that is easy to design a
computer architecture arround.
.FS
We called the Instruction Set,
Sux because it is meant to be very suckless,
or suxless, although, that is not
what Sux is short for.
.FE
.FS
It was also named after a preposed x86 based CPU,
which would have been called, Sux86less.
.FE
.FS
It was never made because we deemed x86,
too bloated for our purposes.
.FE
.FS
It is also meant to be a joke on RISC-V,
in the sense that, like RISC-V,
Sux is small, and simple,
but unlike RISC-V,
Sux is more hobbiest oriented,
rather than being more academic oriented.
.FE
.LP
The Sux ISA does contain documentation for a reference platform,
but the main details of the ISA are not specific to the reference platform.
.\" End of the introduction.
.
.bp +1
.\" Start of the terminology.
.NH 2
.XN Sux Hardware Platform Terminology
.LP
A Sux hardware platform can contain one,
or more Sux compatible cores along with other supporting cores including,
non-Sux compatible cores, MMUs, device controllers, and much more.
.LP
A component is dubbed a \fIcore\fP if it is a turing complete CPU.
A Sux compatible core might support multiple threads, or diet cores,
and are used in the same way as a standard core.
.LP
A Sux core might have have extra instruction set extensions, or a \fIcoprocessor\fP.
We use the term \fIcoprocessor\fP to refer to any type of external device that
offloads some processing from the main core.
.LP
The system-level organization of a Sux hardware platform can range from a single-core
microcontroller, to a 128 core, 1024 thread CPU, or microcontroller, to a multi-CPU
server node. But also even a system-on-a-chip configuration.
.LP
We'll be using both \fI$\fP to denote a hexadecimal
value, and \fI%\fP to denote a binary value.
.\" End of the terminology.
.
.bp +1
.\" Start of the vectors.
.NH 2
.XN Sux Interrupt Vectors
.LP
The way execution is started in a Sux core, is very similar to older 8 bit ISAs,
in that it uses vectors to denote where it should start executing code for some
type of interrupt.
.LP
By default, if there are multiple Sux cores, and each core has multiple threads,
it will start the first thread, of the first core, and then read one of those
vectors.
.LP
Figure 1 shows the layout of the vectors for a Sux core.
.TS
tab(;) allbox;
lb lb
l l.
Address;Name
T{
$FF50-$FF57
.br
$FF58-$FF5F
.br
$FF60-$FF67
.br
$FF60-$FF6F
.br
$FF70-$FF77
.br
$FF70-$FF7F
.br
$FF80-$FF87
T};Thread Vector
T{
$FF90-$FF97
T};\[*m]DCROM Vector
T{
$FFA0-FFA7
T};IRQ Vector
T{
$FFC0-$FFC7
T};Reset Vector
T{
$FFE0-FFE7
T};BRK Vector
.TE
Figure 1: The vectors of Sux.

.LP
All Sux threads can have their execution be suspended by a
wait-for-interrupt, or \fCWAI\fP instuction.
.\" End of the vectors
.
.bp +1
.\" Start of the overview.
.NH 2
.XN Sux ISA Overview
.LP
The Sux ISA is defined as a base integer ISA, which is required
to be present in any implementation, plus any optional extensions to the base ISA.
The base ISA is very similar to MOS Technology's 6502 ISA, except that it
has a 64 bit data bus, 64 bit address bus, supports multiple threads,
multiple cores, and much more.
.LP
Sux has been designed to make using external hardware, very easy to implement.
Sux has also been designed to make customizing, very easy, by having a
Microdecode ROM, or \[*m]DCROM for short. The \[*m]DCROM is covered in more detail
later on.
.LP
Beyond the base ISA, we deem it unnecessary to add any extra opcodes, or
insturctions to it, and as such, will be unchanged, from this point on.
There will still be other extensions, but those will not be accessed in the
manner as the base ISA.
.\" End of the overview.
.
.bp +1
.\" Start of the address space.
.NH 2
.XN Memory
.LP
A Sux thread has a byte-addressable address space of 2\*[{]64\*[}]
bytes for all memory accesses. A \fIword\fP of memory is defined as
16 bits (2 bytes), a \fIdoubleword\fP of memory is defined as
32 bits (4 bytes), and a \fIquadword\fP of memory is defined as 64 bits (8 bytes).
The address space is circular, so that the byte at address 2\*[{]64\*[}]-1
is adjacent to the byte at address zero. Accordingly, memory address computations
done by the hardware ignore overflow, and instead wrap arround modulo 2\*[{]64\*[}].
.LP
The mapping of hardware resources, is determined by the
architecture of the hardware. These addresses may either
(a) be open bus, (b) contain main memory, (c) be vacant,
or (d) contain one, or more I/O devices. Reading, and writing
to I/O devices may have visible side effects, but accessing
main memory cannot. While you could have an architecture that
only accesses I/O, it is usually expected that some of the
address space will be main memory.
.LP
When a Sux platform has multiple threads, unless an MMU is used,
the entire address space must be shared with all threads.
.LP
Executing each Sux machine instruction, most of the time
requires one memory access, but sometimes it might require
two memory accesses. These instructions are divided into
multiple addressing modes.
.\" End of the address space.
