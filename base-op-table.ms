.ce
Table 1. Sux base opcode table
.ps -4.25
.TS
center tab(;) allbox;
l cb cb cb cb cb cb cb cb cb cb cb cb cb cb cb cb
cb c c c c c c c c c c c c c c c.
;x0;x1;x2;x3;x4;x5;x6;x7;x8;x9;xA;xB;xC;xD;xE;xF
0x;CPS;ADC #;AAB;ADC a;JMP ind;ADC zm;PHB;;PHP;LDA #;LDY #;LDX #;TAB;;LDB #;
1x;JMP a;SBC #;SAB;SBC a;JMP inx;SBC zm;PLB;;PLP;LDA a;LDY a;LDX a;TBA;;LDB a;
2x;JSR;AND #;ABA;AND a;JMP iny;AND zm;CPB #;;STT;STA a;STY a;STX a;TAY;;STB a;
3x;BPO;ORA #;OAB;ORA a;JSR ind;ORA zm;CPB a;;SEI;LDA zm;LDY zm;LDX zm;TYA;;LDB zm;
4x;BNG;XOR #;XAB;XOR a;JSR inx;XOR zm;CPB zm;;CLI;STA zm;STY zm;STX zm;TAX;;STB zm;
5x;BCS;LSL #;LLB;LSL a;JSR iny;LSL zm;CPB ind;;SEC;LDA zmx;LDY zmx;LDX zmy;TXA;;LDB zmx;
6x;BCC;LSR #;LRB;LSR a;BPO zm;LSR zm;CPB inx;;CLC;STA zmx;STY zmx;STX zmy;TYX;;STB zmx;
7x;BEQ;ROL #;RLB;ROL a;BNG zm;ROL zm;CPB iny;;SSP;LDA zmy;LDY ind;LDX ind;TXY;;LDB zmy;
8x;BNE;ROR #;RRB;ROR a;BCS zm;ROR zm;INY;;CSP;STA zmy;STY ind;STX ind;TSX;;STB zmy;
9x;BVS;MUL #;MAB;MUL a;BCC zm;MUL zm;DEY;;SEV;LDA ind;LDY inx;LDX iny;TXS;;LDB ind;
Ax;BVC;DIV #;DAB;DIV a;BEQ zm;DIV zm;INX;;CLV;STA ind;STY inx;STX iny;PHY;;STB ind;
Bx;RTS;CMP #;CAB;CMP a;BNE zm;CMP zm;DEX;;ENT;LDA inx;CPY #;CPX #;PLY;;LDB inx;
Cx;RTI;INC A;IAB;INC a;BVS zm;INC zm;;;WAI;STA inx;CPY a;CPX a;PHX;;STB inx;
Dx;JMP zm;DEC A;DBA;DEC a;BVC zm;DEC zm;;;;LDA iny;CPY zm;CPX zm;PLX;;LDB iny;
Ex;JSL;ASR #;ARB;ASR a;;ASR zm;;;NOP;STA iny;CPY ind;CPX ind;PHA;;STB iny;
Fx;RTL;CMP ind;;CMP inx;;CMP iny;;;BRK;;CPY inx;CPX iny;PLA;;;
.TE
.ps
.LP
"#" means Immediate data.
.br
"a" means Absolute addressing.
.br
"zm" means Zero Matrix, which refers to
the first 4 GiB of the address space.
.br
"zmx" means Zero Matrix, indexed with the
X register.
.br
"zmy" means Zero Matrix, indexed with the
Y register.
.br
"ind" means Indirect addressing, Also known
as pointer addressing.
.br
"inx" means Indexed Indirect addressing.
.br
"iny" means Indirect Indexed addressing.
.br
"A" means Accumulator.
.br
And no operand means implied addressing.
