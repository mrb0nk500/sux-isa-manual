.\" Start of the Base ISA.
.NH
.XN The Sux Base Instruction Set, Version 1.0
.LP
This chapter describes version 1.0 of the Sux base instruction set.
.bp +1
.\" Start of the programmers' model.
.NH 2
.XN Programmers' Model for the Base ISA
.LP
Figure 2 Shows the state for the base ISA. The base ISA only has
four registers, each of which are 64 bits in size, each register
has a special property, but can be still be used like general
purpose registers.
.LP
The accumulator, or A is the main register of the base ISA,
and is used for all ALU based instructions. the B register
is a secondary ALU register, and can be used in place of a
memory address for all ALU instructions, the X register is
a register that can be used for indexing, but has a special
property, as it is the only register that can set the stack
pointer, and finally, the Y register, like the X register
can be used for indexing, both normal, and pointer based.
.begin dformat
style bitwid .04
style recht .1618033
style recspread 0
noname
	63-0-64 A
noname
	63-0-64 B
noname
	63-0-64 X
noname
	63-0-64 Y
.end
.ce
Figure 2: Sux base register state.
.\" End of the programmers' model.
.
.bp +1
.\" Start of instruction format.
.NH 2
.XN Base Instruction Format
.LP
The base Sux ISA has variable length instructions ranging
from a single byte, to as many as 10 bytes, and are always
byte aligned. The base ISA is also little endian, with no
possiblity for bi-endian setups.
.LP
The reasoning for why we chose little endian, was because it
is much simpler to work with, plus, it is very easy to understand,
and is very intuitive.
.LP
The high code density is achived by using a prefix byte,
which is not part of the instruction, but rather tells
the CPU extra information like, how many bytes to read/write,
whether it should use 8/16 bit addresses, or 32/64 bit addresses,
or what instruction set extension it should use.
.LP
The base ISA is made up of 198 total opcodes, comprised of
93 instructions. All of the opcodes are one byte in length.
The max operand count for the base ISA is one. These might
look like typos, but all of them were done on purpose, both
in order to make the base ISA easier to understand, and to
keep the opcode length at one byte.
.LP
The instruction formatting for the Sux base ISA is shown in Figure 2.1.
.begin dformat
style bitwid .08
style recht .3
style recspread 0
noname
	71-40-32 opr[63:32]
	39-24-16 opr[31:16]
	23-16-8 opr[15:8]
	15-8-8 opr[7:0]
	7-0-8 opcode
	7-0-8-dashed prefix
.end
Figure 2.1: Sux base instruction format. There are subfields
for the operand because the prefix byte can specify the size
of the operand, from 8 bits, all the way up to 64 bits.
.LP
The processor status register, is a register that contains
flags for use with conditional branches, Figure 2.2 shows
the layout of these flags, the processor status register
is 64 bits in size, but only eight bits are used, this is
so that we can have upto eight sets of these flags, and
make conditional branching with threads, much easier.
.LP
The Carry flag, or C denotes when a register has carried over.
The Zero flag, or Z denotes when a register is zero.
The Interrupt flag, or I is used to disable/enable
maskable interrupts, when set, it disables interrupts,
when cleared, it enables interrupts.
The Stack protection flag, or S is used to prevent stack
overflows, and stack underflows, by not incrementing, or
decrementing if the stack pointer is at it's lowest value,
or it's highest value.
The Overflow flag, or V denotes whenever an overflow of a
register occurs.
And the Negative flag, or N is used to denote a negative
value, and allows the use of signed integers.
.begin dformat
style bitwid .5
style recspread 0
noname
	7-7-1 N
	6-6-1 V
	--1
	--1
	3-3-1 S
	2-2-1 I
	1-1-1 Z
	0-0-1 C
.end
.ce
Figure 2.2: The flags of the Processor status register.
.\" End of instruction format
.
.bp +1
.\" Start of instruction opcodes.
.NH 2
.XN Base Instruction Opcodes
.LP
This section describes each of the instructions, for the Sux base
ISA, and provides a table, listing the opcodes for every instruction.
.so base-op-table.ms
.bp +1
.so base-op-desc.ms
.\" End of instruction opcodes.
.
.bp +1
.\" Start of the prefix byte.
.NH 2
.XN The Prefix Byte
.LP
As mentioned before, there is a prefix byte. The prefix byte
is a byte that occurs before the opcode, and gives the CPU
more control over things like, the number of bytes that it will
read/write, switching between 8/16 bit addressing, and 32/64 bit
addressing, and switching between different instruction set extensions.
.LP
Figure 2.3 shows the layout of the prefix byte.
.br
Table 2 explains what the prefix bits do.
.begin dformat
style bitwid .5
style recspread 0
noname
	7-7-1 EX1
	6-6-1 EX0
	5-5-1 RS1
	4-4-1 RS0
	3-3-1 AM
	2-2-1 1
	1-1-1 1
	0-0-1 1
.end
.ce
Figure 2.3: The control bits of the prefix byte.

.ce
Table 2. Description of the prefix bits
.TS
center tab(;) allbox;
lb lb
l l
l l
lb lb
l l
l l
l l
l l
lb lb
l l
l l
l l
l l.
AM;Addressing Modes
0;Normal Addressing Modes (8/16 bit)
1;Extended Addressing Modes (32/64 bit)
RS1/RS0;Register Size
0/0;8 bit register
0/1;16 bit register
1/0;32 bit register
1/1;64 bit register
EX1/EX0;Extension Selection
0/0;Base ISA
0/1;GFsuX Graphics Oriented Extension
1/0;Unused
1/1;\[*m]DCROM
.TE

.LP
If no prefix byte is found, then it treats that byte as an
opcode, and acts as if it had a prefix byte, with all the
prefix bits set to 0.
.\" End of the prefix byte.
.
.bp +1
.\" Start of assembly syntax.
.NH 2
.XN Assembly Language Syntax for the Sux Base ISA
.LP
The syntax of Sux assembly, was designed to be simple,
and very easy to understand. The syntax looks like this.
.ce
ADC.W #$1000
This is what it means.
.TS
tab(;) allbox;
lb l.
ADC;Instruction
\.W;Register Size Suffix
#$1000;Operand
.TE
.LP
For the Register Size Suffixes, there are four options.
.TS
tab(;) allbox;
lb l.
None;One byte (8 bits)
\.W/\.2;Two bytes (16 bits)
\.D/\.4;Four bytes (32 bits)
\.Q/\.8;Eight bytes (64 bits)
.TE
.LP
These Suffixes control the prefix byte. If there is no suffix,
then it will not place a prefix byte, saving a byte.
.LP
The operand can be a few things.
.TS
tab(;) allbox;
lb lb
l l.
Syntax;Description
Value;Decimal Address
$<Value>;Hexadecimal Address
%<Value>;Binary Address
#[token]<Value>;Immediate Data Value
[token]<Value>, <X/Y>;Zero Matrix, Indexed with X, or Y
([token]<Value>);Indirect Addressing
([token]<Value>, X);Indexed Indirect Addressing
([token]<Value>), Y;Indirect Indexed Addressing
.TE
.\" End of assembly syntax.
.
.bp +1
.\" Start of the MicroDeCodeROM.
.NH 2
.XN The \[*m]DCROM
.LP
The \[*m]DCROM is a type of Programmable Logic Array (PLA) that has each of it's interconnects
connected to an SRAM bit. This SRAM can then be accessed just like any other part of memory,
and as such, is fully reprogrammable by the user.
.LP
The starting location of the \[*m]DCROM, is value located at the \[*m]DCROM vector.
.LP
The byte before the start of the \[*m]DCROM is the control byte, which is used
to specify how the \[*m]DCROM should be accessed.
.\" End of the MicroDeCodeROM.
.\" End of the Base ISA.
