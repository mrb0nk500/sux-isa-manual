.\" Start of SuRP, the Sux Reference Platform.
.NH
.XN The Sux Reference Platform
.LP
The Sux Reference Platform, or SuRP, is the reference architecture for
Sux, and what most other architectures should be based on.
.LP
SuRP will have two boot firmwares, one called SuBAsm, or
the Sux Bootstrapped Assembler, and a bootstrapped version
of the Tiny C Compiler, called SuBTinyCC.
.LP
SuRP will have an MMU chip specifically designed for Sux, called the
MinMMU, a PCI Express controller called MinPCIE, and an RS-232 controller
called the Min232.
.LP
The reasoning for using an assembler, and a C compiler as boot firmware,
was because we wanted to make sure that no one could run prorpietary
code on any Sux based system, and to allow for a possible Maximite like
computer, called the SuRPimite, which will use a microcontroller variant
of Sux, called MicroSux.
.LP
MicroSux will come with SuBAsm, and SuBTinyCC built-in to the chip, but
will still be on a flash chip, so that you can update it, but cannot be
directly accessed by the user, and as such, will have a second flash for
user programs.
