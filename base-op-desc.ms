.NH 3
.XN Descriptions for each instruction of the Sux Base ISA
.LP
.tl 'CPS''Clear Processor Status register.'
.tl 'Opcode''$00'
.LP
The CPS instruction clears all 64 bits of the processor status register.

.LP
.tl 'ADC''ADd with Carry.'
.tl 'Opcodes''#=$01 a=$03 zm=$05'
.tl 'Flags''N V      Z C'
.LP
The ADC instruction adds the operand with the accumulator, and then
stores it back into the accumulator. If the carry flag is set, then
it adds the carry flag along with the sum.

.LP
.tl 'AAB''Add Accumulator, and B with carry.'
.tl 'Opcode''$02'
.tl 'Flags''N V      Z C'
.LP
The AAB instruction is just like the ADC instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'PHB''PusH the B register.'
.tl 'Opcode''$06'
.LP
The PHB instruction pushes the number of bytes specified, from the B
register, onto the stack.

.LP
.tl 'PHP''PusH the Processor status register.'
.tl 'Opcode''$08'
.LP
The PHP instruction pushes the number of bytes specified, from the
processor status register, onto the stack.

.LP
.tl 'LDA''LoaD Accumulator.'
.tl 'Opcodes''#=$09 a=$19 zm=$39 zmx=$59 zmy=$79 ind=$99 inx=$B9 iny=$D9'
.tl 'Flags''N        Z  '
.LP
The LDA instruction loads the value from the operand, into the Accumulator.
It also sets the N, and Z flags.

.LP
.tl 'LDY''LoaD value into Y register.'
.tl 'Opcodes''#=$0A a=$1A zm=$3A zmx=$5A ind=$7A inx=$9A'
.tl 'Flags''N        Z  '
.LP
The LDY instruction loads the value from the operand, into the Y register.
It also sets the N, and Z flags.

.LP
.tl 'LDX''LoaD value into X register.'
.tl 'Opcodes''#=$0B a=$1B zm=$3B zmx=$5B ind=$7B inx=$9B'
.tl 'Flags''N        Z  '
.LP
The LDX instruction loads the value from the operand, into the X register.
It also sets the N, and Z flags.

.LP
.tl 'TAB''Transfer value from the Accumulator, to the B register.'
.tl 'Opcode''$0C'
.tl 'Flags''N        Z  '
.LP
The TAB instruction transfers the value that is in the Accumulator, to the
B register. It also sets the N, and Z flags.
.bp +1
.LP
.tl 'LDB''LoaD value into B register.'
.tl 'Opcodes''#=$0E a=$1E zm=$3E zmx=$5E zmy=$7E ind=$9E inx=$BE iny=$DE'
.tl 'Flags''N        Z  '
.LP
The LDB instruction loads the value from the operand, into the B register.
It also sets the N, and Z flags.

.LP
.tl 'JMP''JuMP to memory address.'
.tl 'Opcodes''a=$10 zm=$D0 ind=$04 inx=$14 iny=$24'
.LP
The JMP instruction sets the program counter to the address, specified by
the operand.

.LP
.tl 'SBC''SuBtract with Carry.'
.tl 'Opcodes''#=$11 a=$13 zm=$15'
.tl 'Flags''N V      Z C'
.LP
The SBC instruction subtracts the operand with the accumulator, and then
stores the result back into the accumulator. If the carry flag is set,
then it borrows from the carry flag along with the sum.

.LP
.tl 'SAB''Subtract Accumulator, and B with carry.'
.tl 'Opcode''$12'
.tl 'Flags''N V      Z C'
.LP
The SAB instruction is just like the SBC instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'PLB''PuLl value from the stack, into the B register.'
.tl 'Opcode''$16'
.LP
The PLB instruction pulls/pops the number of bytes specified, off the
stack, and into the B register.

.LP
.tl 'PLP''PuLl value from the stack, into the Processor status register.'
.tl 'Opcode''$18'
.LP
The PLB instruction pulls/pops the number of bytes specified, off the
stack, and into the Processor status register.

.LP
.tl 'TBA''Transfer value from the B register to the Accumulator.'
.tl 'Opcode''$1C'
.tl 'Flags''N        Z  '
.LP
The TBA instruction transfers the value that is in the B register, to the
Accumulator. It also sets the N, and Z flags.

.LP
.tl 'JSR''Jump to SubRoutine.'
.tl 'Opcodes''zm=$20 ind=$34 inx=$44 iny=$54'
.LP
The JSR instruction pushes the current value of the program counter, onto
the stack, and then sets the program counter to the address, specified by
the operand.

.LP
.tl 'AND''bitwise AND accumulator.'
.tl 'Opcodes''#=$21 a=$23 zm=$25'
.tl 'Flags''N        Z  '
.LP
The AND instruction takes an operand, and ANDs it with the accumulator,
then saves the results back into the accumulator.
.bp +1
.LP
.tl 'ABA''bitwise And Accumulator, with the B register.'
.tl 'Opcode''$22'
.tl 'Flags''N        Z  '
.LP
The ABA instruction is just like the AND instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'CPB''ComPare the B register, with an operand.'
.tl 'Opcodes''#=$26 a=$36 zm=$46 ind=$56 inx=$66 iny=$76'
.tl 'Flags''N V      Z C'
.LP
The CPB instruction compares the value in the B register, with an operand,
and sets the flags accordingly. It compares the two values by subtracting
them.

.LP
.tl 'STT''STart a Thread.'
.tl 'Opcode''$28'
.LP
The STT instruction, sets the current running threads register, and
then has the threads read their thread vector. The current running
threads register is an 8 bit register, that states which threads
are running currently, the first 7 bits of the register states
which of the 7 other threads are running, the remaining 16 bits
of the operand, if specified, are used to tell the instruction
which core to start, and are read bytewise, rather than bitwise.
This instruction will not be implemented, if the core
count, and thread count are both 1, otherwise, it will.

.LP
.tl 'STA''STore the Accumulator, into memory.'
.tl 'Opcodes''a=$29 zm=$49 zmx=$69 zmy=$89 ind=$A9 inx=$C9 iny=$E9'
.LP
The STA instruction writes the value of the Accumulator, into a memory
address.

.LP
.tl 'STY''STore the Y register, into memory.'
.tl 'Opcodes''a=$2A zm=$4A zmx=$6A ind=$8A inx=$AA'
.LP
The STY instruction writes the value of the Y register, into a memory
address.

.LP
.tl 'STX''STore the X register, into memory.'
.tl 'Opcodes''a=$2B zm=$4B zmx=$6B ind=$8B inx=$AB'
.LP
The STX instruction writes the value of the X register, into a memory
address.

.LP
.tl 'TAY''Transfer value from the Accumulator, to the Y register.'
.tl 'Opcode''$2C'
.tl 'Flags''N        Z  '
.LP
The TAY instruction transfers the value that is in the Accumulator, to the
Y register. It also sets the N, and Z flags.

.LP
.tl 'STB''STore the B register, into memory.'
.tl 'Opcodes''a=$2E zm=$4E zmx=$6E zmy=$8E ind=$AE inx=$CE iny=$EE'
.LP
The STB instruction writes the value of the B register, into a memory
address.

.LP
.tl 'BPO''Branch if POsitive.'
.tl 'Opcodes''a=$30 zm=$64'
.LP
The BPO instruction will branch, if the Negative flag is cleared.
.bp +1
.LP
.tl 'ORA''bitwise OR Accumulator.'
.tl 'Opcodes''#=$31 a=$33 zm=$35'
.tl 'Flags''N        Z  '
.LP
The ORA instruction takes an operand, and ORs it with the accumulator,
then saves the results back into the accumulator.

.LP
.tl 'OAB''bitwise Or Accumulator, with the B register.'
.tl 'Opcode''$32'
.tl 'Flags''N        Z  '
.LP
The OAB instruction is just like the ORA instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'SEI''SEt the Interrupt flag.'
.tl 'Opcode''$38'
.tl 'Flags''        I    '
.LP
The SEI instruction, sets the Interrupt flag, and thus disabling
maskable interrupts.

.LP
.tl 'TYA''Transfer value from the Y register to the Accumulator.'
.tl 'Opcode''$3C'
.tl 'Flags''N        Z  '
.LP
The TYA instruction transfers the value that is in the Y register, to the
Accumulator. It also sets the N, and Z flags.

.LP
.tl 'BNG''Branch if NeGative.'
.tl 'Opcodes''a=$40 zm=$74'
.LP
The BNG instruction will branch, if the Negative flag is set.

.LP
.tl 'XOR''bitwise XOR accumulator.'
.tl 'Opcodes''#=$41 a=$43 zm=$45'
.tl 'Flags''N        Z  '
.LP
The XOR instruction takes an operand, and XORs it with the accumulator,
then saves the results back into the accumulator.

.LP
.tl 'XAB''bitwise Xor Accumulator, with the B register.'
.tl 'Opcode''$42'
.tl 'Flags''N        Z  '
.LP
The XAB instruction is just like the XOR instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'CLI''CLear the Interrupt flag.'
.tl 'Opcode''$48'
.tl 'Flags''        I    '
.LP
The CLI instruction, clears the Interrupt flag, and thus enabling
maskable interrupts.

.LP
.tl 'TAX''Transfer value from the Accumulator to the X register.'
.tl 'Opcode''$4C'
.tl 'Flags''N        Z  '
.LP
The TAX instruction transfers the value that is in the Accumulator, to the
X register. It also sets the N, and Z flags.

.LP
.tl 'BCS''Branch if Carry Set.'
.tl 'Opcodes''a=$50 zm=$84'
.LP
The BCS instruction will branch, if the Carry flag is set.

.LP
.tl 'LSL''Logical Shift Left accumulator.'
.tl 'Opcodes''#=$51 a=$53 zm=$55'
.tl 'Flags''N        Z C'
.LP
The LSL instruction takes an operand, and shifts the accumulator left by
the number of bits from the operand, then saves the results back into the
accumulator. The Carry flag is set whenever a one is shifted out of the
accumulator.

.LP
.tl 'LLB''Logical shift Left accumulator, with the B register.'
.tl 'Opcode''$52'
.tl 'Flags''N        Z C'
.LP
The LLB instruction is just like the LSL instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'SEC''SEt the Carry flag.'
.tl 'Opcode''$58'
.tl 'Flags''            C'
.LP
The SEC instruction, sets the Carry flag.

.LP
.tl 'TXA''Transfer value from the X register to the Accumulator.'
.tl 'Opcode''$5C'
.tl 'Flags''N        Z  '
.LP
The TXA instruction transfers the value that is in the X register, to the
Accumulator. It also sets the N, and Z flags.

.LP
.tl 'BCC''Branch if Carry Clear.'
.tl 'Opcodes''a=$60 zm=$94'
.LP
The BCC instruction will branch, if the Carry flag is cleared.

.LP
.tl 'LSR''Logical Shift Right accumulator.'
.tl 'Opcodes''#=$61 a=$63 zm=$65'
.tl 'Flags''N        Z C'
.LP
The LSR instruction takes an operand, and shifts the accumulator right by
the number of bits from the operand, then saves the results back into the
accumulator. The Carry flag is set whenever a one is shifted out of the
accumulator.

.LP
.tl 'LRB''Logical shift Right accumulator, with the B register.'
.tl 'Opcode''$62'
.tl 'Flags''N        Z C'
.LP
The LRB instruction is just like the LSR instruction, except that
it uses the B register as an operand, rather than memory.
.bp +1
.LP
.tl 'CLC''CLear the Carry flag.'
.tl 'Opcode''$68'
.tl 'Flags''            C'
.LP
The CLC instruction, clears the Carry flag.

.LP
.tl 'TYX''Transfer value from the Y register to the X register.'
.tl 'Opcode''$6C'
.tl 'Flags''N        Z  '
.LP
The TYX instruction transfers the value that is in the Y register, to the
X register. It also sets the N, and Z flags.

.LP
.tl 'BEQ''Branch if EQual.'
.tl 'Opcodes''a=$70 zm=$A4'
.LP
The BEQ instruction will branch, if the Zero flag is set.

.LP
.tl 'ROL''ROtate Left accumulator.'
.tl 'Opcodes''#=$71 a=$73 zm=$75'
.tl 'Flags''N        Z C'
.LP
The ROL instruction takes an operand, and rotates the accumulator left by
the number of bits from the operand, then saves the results back into the
accumulator. The Carry flag is set whenever a one is shifted out of the
accumulator.

.LP
.tl 'RLB''Rotate Left accumulator, with the B register.'
.tl 'Opcode''$72'
.tl 'Flags''N        Z C'
.LP
The RLB instruction is just like the ROL instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'SSP''Set the Stack Protection flag.'
.tl 'Opcode''$78'
.tl 'Flags''      S      '
.LP
The SSP instruction, sets the Stack protection flag.

.LP
.tl 'TXY''Transfer value from the X register to the Y register.'
.tl 'Opcode''$7C'
.tl 'Flags''N        Z  '
.LP
The TXY instruction transfers the value that is in the X register, to the
Y register. It also sets the N, and Z flags.
.bp +1
.LP
.tl 'BNE''Branch if Not Equal.'
.tl 'Opcodes''a=$80 zm=$B4'
.LP
The BNE instruction will branch, if the Zero flag is cleared.

.LP
.tl 'ROR''ROtate Right accumulator.'
.tl 'Opcodes''#=$81 a=$83 zm=$85'
.tl 'Flags''N        Z C'
.LP
The ROR instruction takes an operand, and rotates the accumulator right by
the number of bits from the operand, then saves the results back into the
accumulator. The Carry flag is set whenever a one is shifted out of the
accumulator.

.LP
.tl 'RRB''Rotate Right accumulator, with the B register.'
.tl 'Opcode''$82'
.tl 'Flags''N        Z C'
.LP
The RRB instruction is just like the ROR instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'INY''INcrement the Y register.'
.tl 'Opcode''$86'
.tl 'Flags''N        Z  '
.LP
The INY instruction increments the Y register.

.LP
.tl 'CSP''Clear the Stack Protection flag.'
.tl 'Opcode''$88'
.tl 'Flags''      S      '
.LP
The CSP instruction, clears the Stack protection flag.

.LP
.tl 'TSX''Transfer value from the Stack pointer to the X register.'
.tl 'Opcode''$8C'
.tl 'Flags''N        Z  '
.LP
The TSX instruction transfers the value that is in the Stack pointer, to the
X register. It also sets the N, and Z flags.

.LP
.tl 'BVS''Branch if oVerflow Set.'
.tl 'Opcodes''a=$90 zm=$C4'
.LP
The BVS instruction will branch, if the Overflow flag is set.

.LP
.tl 'MUL''MULtiply accumulator, by operand.'
.tl 'Opcodes''#=$91 a=$93 zm=$95'
.tl 'Flags''N V      Z C'
.LP
The MUL instruction takes an operand, and multiplys the accumulator by the
operand, then saves the results back into the accumulator. If the Carry flag
is set, then it will add one to the result.

.LP
.tl 'MAB''Multiply Accumulator, with the B register.'
.tl 'Opcode''$92'
.tl 'Flags''N V      Z C'
.LP
The MAB instruction is just like the MUL instruction, except that
it uses the B register as an operand, rather than memory.
.bp +1
.LP
.tl 'DEY''DEcrement the Y register.'
.tl 'Opcode''$96'
.tl 'Flags''N        Z  '
.LP
The DEY instruction decrements the Y register.

.LP
.tl 'SEV''SEt the oVerflow flag.'
.tl 'Opcode''$98'
.tl 'Flags''  V          '
.LP
The SEV instruction, sets the Overflow flag.
.LP
.tl 'TXS''Transfer value from the X register to the Stack pointer.'
.tl 'Opcode''$9C'
.tl 'Flags''N        Z  '
.LP
The TXS instruction transfers the value that is in the X register, to the
Stack pointer, if the prefix byte has a high nibble of $1, then it will turn
into an immediate data instruction, and the operand will set the Stack Bank
Register, the SBR is a 16 bit register that states where the starting address
of the Stack pointer is. It also sets the N, and Z flags.

.LP
.tl 'BVC''Branch if oVerflow Clear.'
.tl 'Opcodes''a=$A0 zm=$D4'
.LP
The BVC instruction will branch, if the Overflow flag is cleared.

.LP
.tl 'DIV''DIVide accumulator, by operand.'
.tl 'Opcodes''#=$A1 a=$A3 zm=$A5'
.tl 'Flags''N V      Z  '
.LP
The DIV instruction takes an operand, and divides the accumulator by the
operand, then saves the results back into the accumulator.

.LP
.tl 'DAB''Divide Accumulator, with the B register.'
.tl 'Opcode''$A2'
.tl 'Flags''N V      Z  '
.LP
The DAB instruction is just like the DIV instruction, except that
it uses the B register as an operand, rather than memory. It also
let's you dab on all those lesser ISAs.

.LP
.tl 'INX''INcrement the X register.'
.tl 'Opcode''$A6'
.tl 'Flags''N        Z  '
.LP
The INX instruction increments the X register.

.LP
.tl 'CLV''CLear the oVerflow flag.'
.tl 'Opcode''$A8'
.tl 'Flags''  V          '
.LP
The CLV instruction, clears the Overflow flag.

.LP
.tl 'PHY''PusH the Y register.'
.tl 'Opcode''$AC'
.LP
The PHY instruction pushes the number of bytes specified, from the Y
register, onto the stack.

.LP
.tl 'RTS''ReTurn from Subroutine.'
.tl 'Opcodes''$B0'
.LP
The RTS instruction is the opposite of the JSR instruction,
in that it pulls from the stack, the address that JSR had pushed,
and places it in the program counter.

.LP
.tl 'CMP''CoMPare accumulator, with operand.'
.tl 'Opcodes''#=$B1 a=$B3 zm=$B5 ind=$F1 inx=$F3 iny=$F5'
.tl 'Flags''N V      Z C'
.LP
The CMP instruction compares the value in the B register, with an operand,
and sets the flags accordingly. It compares the two values by subtracting
them.

.LP
.tl 'CAB''Compare Accumulator, with the B register.'
.tl 'Opcode''$B2'
.tl 'Flags''N V      Z C'
.LP
The CAB instruction is just like the CMP instruction, except that
it uses the B register as an operand, rather than memory.

.LP
.tl 'DEX''DEcrement the X register.'
.tl 'Opcode''$B6'
.tl 'Flags''N        Z  '
.LP
The DEX instruction decrements the X register.

.LP
.tl 'ENT''ENd a Thread.'
.tl 'Opcode''$B8'
.LP
The ENT instruction, clears the bits in the Current Running Threads register,
specified by the operand.

.LP
.tl 'CPY''ComPare the B register, with an operand.'
.tl 'Opcodes''#=$BA a=$CA zm=$DA ind=$EA inx=$FA'
.tl 'Flags''N V      Z C'
.LP
The CPY instruction compares the value in the Y register, with an operand,
and sets the flags accordingly. It compares the two values by subtracting
them.

.LP
.tl 'CPX''ComPare the X register, with an operand.'
.tl 'Opcodes''#=$BB a=$CB zm=$DB ind=$EB inx=$FB'
.tl 'Flags''N V      Z C'
.LP
The CPX instruction compares the value in the X register, with an operand,
and sets the flags accordingly. It compares the two values by subtracting
them.

.LP
.tl 'PLY''PuLl value from the stack, into the Y register.'
.tl 'Opcode''$BC'
.LP
The PLY instruction pulls/pops the number of bytes specified, off the
stack, and into the Y register.

.LP
.tl 'RTI''ReTurn from Interrupt.'
.tl 'Opcodes''$C0'
.LP
The RTI instruction is like the RTS instruction, in that it pulls from the stack,
the previous value of the processor status register, and the previous address that
had been pushed from an interrupt, and places them into, the processor status register,
and the program counter, respectivly.
.bp +1
.LP
.tl 'INC''INCrement accumulator, or memory address.'
.tl 'Opcodes''A=$C1 a=$C3 zm=$C5'
.tl 'Flags''N        Z  '
.LP
The INC instruction increments either the accumulator, or the contents of a
memory address.

.LP
.tl 'IAB''Increment Accumulator, with the B register.'
.tl 'Opcode''$C2'
.tl 'Flags''N        Z  '
.LP
The IAB instruction increments both the accumulator, and the B register.

.LP
.tl 'WAI''WAIt for an interrupt.'
.tl 'Opcode''$C8'
.LP
The WAI instruction, puts the CPU into a sleep mode, where it is only
awake enough to be able to respond to an interrupt, and as such, not only
uses less power, but also allows for very fast interrupt response times.

.LP
.tl 'PHX''PusH the X register.'
.tl 'Opcode''$CC'
.LP
The PHX instruction pushes the number of bytes specified, from the X
register, onto the stack.

.LP
.tl 'DEC''DECrement accumulator, or memory address.'
.tl 'Opcodes''A=$D1 a=$D3 zm=$D5'
.tl 'Flags''N        Z  '
.LP
The DEC instruction decrements either the accumulator, or the contents of a
memory address.

.LP
.tl 'DBA''Dncrement Accumulator, with the B register.'
.tl 'Opcode''$D2'
.tl 'Flags''N        Z  '
.LP
The DBA instruction decrements both the accumulator, and the B register.

.LP
.tl 'PLX''PuLl value from the stack, into the X register.'
.tl 'Opcode''$DC'
.LP
The PLX instruction pulls/pops the number of bytes specified, off the
stack, and into the X register.

.LP
.tl 'JSL''Jump to Subroutine, Long addressing.'
.tl 'Opcode''a=$E0'
.LP
The JSL instruction, is just like the JSR instruction, except that it pushes
a 64 bit address onto the stack.

.LP
.tl 'ASR''Arithmetic Shift Right accumulator.'
.tl 'Opcodes''#=$E1 a=$E3 zm=$E5'
.tl 'Flags''N        Z C'
.LP
The ASR instruction takes an operand, and shifts the accumulator right by
the number of bits from the operand, then saves the results back into the
accumulator. But unlike LSR, ASR shifts in the sign bit into the accumulator.
The Carry flag is set whenever a one is shifted out of the accumulator.

.LP
.tl 'ARB''Arithmetic shift Right accumulator, with the B register.'
.tl 'Opcode''$E2'
.tl 'Flags''N        Z C'
.LP
The ARB instruction is just like the ASR instruction, except that
it uses the B register as an operand, rather than memory.
.bp +1
.LP
.tl 'NOP''No OPeration.'
.tl 'Opcode''$E8'
.LP
The NOP instruction does what you think it does, it does nothing at all.

.LP
.tl 'PHA''PusH the Accumulator.'
.tl 'Opcode''$EC'
.LP
The PHA instruction pushes the number of bytes specified, from the
Accumulator, onto the stack.

.LP
.tl 'RTL''ReTurn from subroutine, Long addressing.'
.tl 'Opcodes''$F0'
.LP
The RTL instruction, is just like the RTS instruction, except that it pulls
a 64 bit address from the stack.

.LP
.tl 'BRK''BReaKpoint.'
.tl 'Opcode''$F8'
.LP
The BRK instruction triggers a software interrupt, which pushes the value
in the processor status register, and the address in the program counter,
onto the stack, then reads the BRK vector, and then jumps to the address
located there.

.LP
.tl 'PLA''PuLl value from the stack, into the Accumulator.'
.tl 'Opcode''$FC'
.LP
The PLA instruction pulls/pops the number of bytes specified, off the
stack, and into the accumulator.
